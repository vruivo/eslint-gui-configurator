# ESLint GUI Configurator
  * Visually edit ESLint configuration through a webpage.
  * Webpage ruleset automatically created from ESLint source.

### How to run:
 * go to source-reader/git-clone/ and do a git clone from eslint repo
 * cd eslint
 * npm init
 * back to root cd ../../../
 * node .
 * cd output/
 * open index.html with the browser

### TODO:
 * rewrite using TDD
 * fix array controls
 * save output to a file
 * automatically check eslint git repository for changes and build new version
